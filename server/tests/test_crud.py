from sqlalchemy.orm import Session

from database import schemas, crud, models
from tests.util import test_hike, test_image


def test_get_hike_by_id_none(db_session: Session):
    hike_id = 1
    assert crud.get_hike_by_id(db_session, hike_id) is None


def test_get_image_by_id_none(db_session: Session):
    img_id = 1
    assert crud.get_image_by_id(db_session, img_id) is None


def test_get_hike_by_id(db_session: Session):
    # add data
    db_session.add(models.Hike(name="test_hike"))
    db_session.commit()

    assert crud.get_hike_by_id(db_session, 1)


def test_get_image_by_id(db_session: Session):
    db_session.add((models.Image(file_path="test")))
    db_session.commit()

    assert crud.get_image_by_id(db_session, 1)


def test_get_hikes(db_session: Session):
    db_session.add(models.Hike(name="test_hike"))
    db_session.commit()

    hikes = crud.get_hikes(db_session, 100)
    assert len(hikes) == 1

    db_session.add(models.Hike(name="test_hike2"))
    db_session.commit()

    assert len(crud.get_hikes(db_session, 100)) == 2
    assert len(crud.get_hikes(db_session, 1)) == 1
    assert len(crud.get_hikes(db_session, 0)) == 0


def test_add_hike(db_session: Session):
    crud.add_hike(db_session, schemas.HikeCreate.parse_obj(test_hike))

    hike = db_session.query(models.Hike).filter(models.Hike.name == test_hike["name"]).first()

    assert hike
    assert hike.name == test_hike["name"]


def test_add_image_to_hike(db_session: Session):
    crud.add_hike(db_session, schemas.HikeCreate.parse_obj(test_hike))
    image_id = crud.add_image_to_hike(db_session, schemas.ImageCreate.parse_obj(test_image))

    image = db_session.query(models.Image).filter(models.Image.id == image_id).first()
    assert image


def test_add_images(db_session: Session):
    crud.add_hike(db_session, schemas.HikeCreate.parse_obj(test_hike))
    images = [schemas.ImageCreate.parse_obj(test_image), schemas.ImageCreate.parse_obj(test_image)]
    img_ids = crud.add_images(db_session, images)

    assert len(img_ids) == len(images)
