from pathlib import Path

from fastapi import UploadFile

import api.util


def test_save_file(monkeypatch):
    def mock_file_path(filename):
        return Path().resolve().parent / "uploads/test.png"

    monkeypatch.setattr(api.util, "get_file_path", mock_file_path)

    file_obj = Path("test_img.png").open("r").close()
    upload_file = UploadFile("test_img.png", file_obj, "image/png")

    file_path = api.util.save_file(upload_file)

    assert file_path.is_file()
    # Clean up file
    file_path.unlink()
