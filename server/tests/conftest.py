import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

from api import get_db
from api.main import app
from database.database import Base

SQLALCHEMY_DATABASE_URL = "sqlite:///./"


def pytest_addoption(parser):
    parser.addoption("--db", action="store", default="test.db")


@pytest.fixture(scope="session")
def db_engine(request):
    """Yields a SQLAlchemy engine which is suppressed after the test."""

    db_name = request.config.getoption("--db")
    engine = create_engine(SQLALCHEMY_DATABASE_URL + db_name, connect_args={"check_same_thread": False}, echo=False)
    Base.metadata.create_all(engine)
    yield engine.connect()

    Base.metadata.drop_all(engine)
    engine.dispose()


# @pytest.fixture(scope="session")
# def db_session_factory(db_engine):
#     """Returns a SQLAlchemy scoped session factory"""
#
#     yield scoped_session(sessionmaker(bind=db_engine))

@pytest.fixture(scope="function")
def db_session(db_engine):
    """Yields a SQLAlchemy connection which is rolled back after the test."""
    transaction = db_engine.begin()
    yield scoped_session(sessionmaker(bind=db_engine))

    transaction.rollback()
    transaction.close()


@pytest.fixture()
def client(db_session):
    def _get_test_db():
        try:
            yield db_session
        finally:
            pass

    app.dependency_overrides[get_db] = _get_test_db
    with TestClient(app) as client:
        yield client
