from pathlib import Path

from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

import api.util
from database import models
from tests.util import test_hike, test_image


def test_read_hikes_empty(client: TestClient):
    response = client.get("/hikes/")
    assert response.status_code == 200
    assert response.json() == []


def test_read_hikes(client: TestClient, db_session: Session):
    db_session.add(models.Hike(**test_hike))
    db_session.commit()

    response = client.get("/hikes/")
    assert response.status_code == 200
    hikes_json = response.json()
    assert len(hikes_json) == 1
    assert hikes_json[0]["name"] == test_hike["name"]
    assert hikes_json[0]["completed"] == test_hike["completed"]


def test_read_hike_by_id(client: TestClient, db_session: Session):
    db_session.add(models.Hike(**test_hike))
    db_session.commit()

    response = client.get("/hikes/1")
    assert response.status_code == 200
    json = response.json()
    assert json["id"] == 1


def test_create_hike(client: TestClient):
    test_json = test_hike
    test_json["date_of_hike"] = str(test_json["date_of_hike"])

    response = client.post("/hikes/", headers={}, json=test_json)

    assert response.status_code == 200


def test_upload_images(monkeypatch, client: TestClient):
    test_json = test_hike
    test_json["date_of_hike"] = str(test_json["date_of_hike"])

    response = client.post("/hikes/", headers={}, json=test_json)
    hike_id = response.json()["id"]

    file_path = Path().resolve().parent / "uploads/test.png"

    def mock_file_path(filename):
        return file_path

    monkeypatch.setattr(api.util, "get_file_path", mock_file_path)

    headers = {
        'accept': "application/json",
        "Content-Type": "multipart/form-data"
    }

    files = [
        ("images", ('test.png', open(test_image['file_path'], 'rb'), 'image/png'))
    ]

    res = client.post(f"/hikes/{hike_id}/images", files=files)

    assert res.status_code == 200
    assert len(res.json()) == 1

    file_path.unlink()
