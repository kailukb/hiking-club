from datetime import datetime
from pathlib import Path

test_hike = {
    "name": "test_hike",
    "completed": False,
    "date_of_hike": datetime(2022, 1, 22).date(),
    "location": "moon",
    "images": [],
}

test_image = {
    "hike_id": 1,
    "file_path": str(Path("test_img.png"))
}
