import shutil
import uuid
from pathlib import Path

from fastapi import UploadFile

from util import UPLOAD_FOLDER


def get_file_path(filename: str) -> Path:
    # Generate a unique id for file name
    file_suffix = Path(filename).suffix
    file_name = str(uuid.uuid4()) + file_suffix
    file_path = UPLOAD_FOLDER / file_name

    return file_path


def save_file(upload_file: UploadFile) -> Path:
    """Saves a uploaded file to the server and returns the file_name."""

    destination = get_file_path(upload_file.filename)

    # Save file in the uploads folder
    try:
        with open(destination, "wb+") as buffer:
            shutil.copyfileobj(upload_file.file, buffer)
    finally:
        upload_file.file.close()

    return destination
