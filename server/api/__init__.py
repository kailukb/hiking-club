from database import models
from database.database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)


# Dependency
def get_db():
    db = SessionLocal()  # pragma: no cover
    try:  # pragma: no cover
        yield db
    finally:  # pragma: no cover
        db.close()
