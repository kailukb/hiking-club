from fastapi import Depends, File, UploadFile, APIRouter
from sqlalchemy.orm import Session

from api import get_db
from api import util
from database import schemas, crud

router = APIRouter(prefix="/hikes")


@router.get("/", response_model=list[schemas.Hike])
def read_hikes(limit: int = 10, db: Session = Depends(get_db)):
    hikes = crud.get_hikes(db, limit)
    return hikes


@router.get("/{hike_id}", response_model=schemas.Hike)
def read_hike_by_id(hike_id: int, db: Session = Depends(get_db)):
    hike = crud.get_hike_by_id(db, hike_id)
    return hike


@router.post("/", response_model=schemas.Hike)
def create_hike(hike: schemas.HikeCreate, db: Session = Depends(get_db)):
    db_hike = crud.add_hike(db, hike)
    return db_hike


@router.post("/{hike_id}/images")
def upload_images(hike_id: int, images: list[UploadFile] = File(...), db: Session = Depends(get_db)):
    # save each uploaded image to the server
    saved_images = [util.save_file(f) for f in images]
    # Create our image schemas
    img_schemas = [schemas.ImageCreate.parse_obj(
        {"hike_id": hike_id, "file_path": img.name}) for img in saved_images]
    # Add the schemas to the database
    ids = crud.add_images(db, img_schemas)
    return ids
