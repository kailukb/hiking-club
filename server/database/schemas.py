from datetime import date

from pydantic import BaseModel


class ImageBase(BaseModel):
    hike_id: int
    file_path: str


class ImageCreate(ImageBase):
    pass


class Image(ImageBase):
    id: int

    class Config:
        orm_mode = True


class HikeBase(BaseModel):
    name: str
    completed: bool
    date_of_hike: date
    location: str
    distance: int = None
    duration: str = None
    images: list[Image] = []


class HikeCreate(HikeBase):
    pass


class Hike(HikeBase):
    id: int

    class Config:
        orm_mode = True
