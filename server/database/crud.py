from sqlalchemy.orm import Session

from . import models, schemas


def get_hike_by_id(db: Session, hike_id: int) -> models.Hike:
    """
    Fetch the hike by the id
    :param db: the database session object
    :param hike_id: the id of the hike
    :return: A Hike database object, or None if not found.
    """
    return db.query(models.Hike).filter(models.Hike.id == hike_id).first()


def get_hikes(db: Session, limit: int) -> list[models.Hike]:
    """
    Returns multiple hikes from the database
    :param db: database session
    :param limit: max hikes to return
    :return: list of hikes
    """
    return db.query(models.Hike).limit(limit).all()


def add_hike(db: Session, hike: schemas.HikeCreate) -> models.Hike:
    """
    Adds a new hike to the database.
    :param hike: the hike data to add
    :param db: the database session
    :return: The hike model added to the database.
    """
    db_hike = models.Hike(**hike.dict())
    db.add(db_hike)
    db.commit()
    db.refresh(db_hike)
    return db_hike


def get_image_by_id(db: Session, img_id: int) -> models.Image:
    return db.query(models.Image).filter(models.Image.id == img_id).first()


def add_image_to_hike(db: Session, image: schemas.ImageCreate) -> int:
    db_image = models.Image(**image.dict())
    db.add(db_image)
    db.commit()
    db.refresh(db_image)
    return db_image.id


def add_images(db: Session, images: list[schemas.ImageCreate]) -> list[int]:
    image_models = [models.Image(**img.dict()) for img in images]
    db.add_all(image_models)
    db.commit()
    db_images = db.query(models.Image).filter(models.Image.hike_id == images[0].hike_id).all()
    return [img.id for img in db_images]
