from sqlalchemy import Column, String, Integer, ForeignKey, Boolean, Date
from sqlalchemy.orm import relationship

from .database import Base


class Hike(Base):
    """
    Defines the hike model
    """
    __tablename__ = "hikes"
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    completed = Column(Boolean)
    date_of_hike = Column(Date, None)
    distance = Column(Integer)
    duration = Column(Integer)
    location = Column(String)

    images = relationship('Image', backref="hike")


class Image(Base):
    """Defines the Image model for a hike"""

    __tablename__ = "images"
    id = Column(Integer, primary_key=True)
    file_path = Column(String, default="")
    hike_id = Column(Integer, ForeignKey("hikes.id"))
