const base = "http://localhost:8000";

async function send(method, path, data?, token?) {
    const opts = {method, headers: {}, body: undefined};

    if (data) {
        opts.headers['Content-Type'] = 'application/json';
        opts.body = JSON.stringify(data);
    }

    if (token) {
        opts.headers['Authorization'] = token;
    }
    const endpoint = `${base}/${path}/`;

    return await fetch(endpoint, opts)
        .then((r) => r.text())
        .then((json) => {
            try {
                return JSON.parse(json);
            } catch (err) {
                return json;
            }
        });

}

export async function get(path) {
    return await send("GET", path);
}

export async function post(path, data) {
    return send("POST", path, data);
}